const { app } = require('electron')

const { createMouseMenu, createAppMenu } = require('./components/menu')

const { initInterceptor } = require('./components/interceptor')
const { createWindow } = require('./components/main_window')

createMouseMenu()
createAppMenu()

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) {
  app.quit()
}

createWindow()
initInterceptor()