const axios = require('axios');
const { checkIp } = require('./check_peer')
const { setDefaultIp, getDefaultIp } = require('./default_ip_storage')


function fetchIps(resolve) {
  axios({
    method: 'get',
    url: 'https://gitlab.com/devapro/ip-list/-/raw/main/list.json?inline=false',
    responseType: 'json'
  })
    .then(function (response) {
      console.log(response.data)
      response.data.forEach((ip) => {
        checkIp(ip).then(() => {
          console.log(getDefaultIp());
          if(getDefaultIp() == '') {
            setDefaultIp(ip)
            resolve();
          }
        }).catch((e) => {
          console.log(e);
        });
      });
    }).catch((e) => {
      console.log(e);
      fetchIps(resolve);
    });
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

module.exports = {

   fetchPeers: () => new Promise(resolve => {
    fetchIps(() => {
      sleep(1000).then(() => { resolve() });
    })
  })
}