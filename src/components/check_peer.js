const { getNetworkClient } = require('./network')

module.exports = {

    checkIp: (ip) => {
        return getNetworkClient()({
          method: 'post',
          url: 'https://' + ip + '/prizm?requestType=getAccount',
          responseType: 'json'
        })
      }
}