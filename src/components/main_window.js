const { app, BrowserWindow } = require('electron');
const path = require('path');
const serve = require('electron-serve'); //https://github.com/sindresorhus/electron-serve
const { fetchPeers } = require('./fetch_peers')

function init() {
    const loadURL = serve({
        directory: 'src/PrizmWeb'
      });
      
    const createWindow = () => {
        // Create the browser window.
        const mainWindow = new BrowserWindow({
            width: 800,
            height: 600,
            webPreferences: {
            preload: path.join(__dirname, 'preload.js'),
            },
        });
    
        // and load the index.html of the app.
        mainWindow.loadFile(path.join(__dirname, '../index.html'));
        //mainWindow.loadURL("https://192.168.100.66:7799/wallet/index.html");
    
        fetchPeers().then(() => {
            setTimeout(() => {
                loadURL(mainWindow);
            }, 1000)
        }).catch( e => {
            app.quit();
        })
    
        // Open the DevTools.
        if (process.env.NODE_ENV === 'development') {
            mainWindow.webContents.openDevTools();
        }
    };
    
    // This method will be called when Electron has finished
    // initialization and is ready to create browser windows.
    // Some APIs can only be used after this event occurs.
    app.on('ready', createWindow);
    
    // Quit when all windows are closed, except on macOS. There, it's common
    // for applications and their menu bar to stay active until the user quits
    // explicitly with Cmd + Q.
    app.on('window-all-closed', () => {
        if (process.platform !== 'darwin') {
            app.quit();
        }
    });
    
    app.on('activate', () => {
        // On OS X it's common to re-create a window in the app when the
        // dock icon is clicked and there are no other windows open.
        if (BrowserWindow.getAllWindows().length === 0) {
            createWindow();
        }
    });
}

module.exports = {
    createWindow: () => {
        init()
    }
}