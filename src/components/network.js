const axios = require('axios');
const https = require('https');

const httpsAgent = new https.Agent({
    rejectUnauthorized: false,
  })
  
axios.defaults.httpsAgent = httpsAgent

module.exports = {
    getNetworkClient: () => axios
}