const prompt = require('electron-prompt');
const { dialog } = require('electron');
const { checkIp } = require('./check_peer')
const { setDefaultIp, getDefaultIp } = require('./default_ip_storage')

module.exports = {

    enterIp: () => {
        prompt({
          title: 'Enter node ip and port',
          label: 'URL:',
          value: getDefaultIp(),
          inputAttrs: {
              type: 'text'
          },
          type: 'input'
        })
        .then((r) => {
            if(r === null) {
                console.log('user cancelled');
            } else {
                console.log('result', r);
                checkIp(r).then(() => {
                    setDefaultIp(r)
                }).catch((e) => {
                  dialog.showMessageBox({ title: 'Error', message: 'Wrong IP' });
                });
            }
        })
        .catch(console.error);
      }
}