const { app, protocol, net } = require('electron');
const { getDefaultIp } = require('./default_ip_storage')

function startIntercept() {
    protocol.handle('ppp', async (request) => {
        return net.fetch(
          'https://' + getDefaultIp() + request.url.slice('ppp://host'.length),
          {
            method: request.method,
            headers: request.headers,
            body: request.body,
            duplex: "half"
          }
        );
      });
}

module.exports = {

    initInterceptor: () => {
        protocol.registerSchemesAsPrivileged([
            {
              scheme: 'ppp',
              privileges: {
                standard: true,
                secure: false,
                supportFetchAPI: true
              }
            }
          ]);

          app.commandLine.appendSwitch('ignore-certificate-errors')

          app.on('certificate-error', (event, webContents, url, error, certificate, callback) => {
  
            // Prevent having error
            event.preventDefault()
            // and continue
            callback(true)
          
          });

          app.whenReady().then(() => {
            startIntercept()  
          })
    }
}