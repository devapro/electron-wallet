const contextMenu = require('electron-context-menu');
const { Menu } = require('electron');
const { enterIp } = require('./enter_ip_promt')

module.exports = {

    createMouseMenu: () => {
        contextMenu({
            menu: (actions, props, browserWindow, dictionarySuggestions) => [
                ...dictionarySuggestions,
                actions.separator(),
                actions.copy(),
                actions.separator(),
                actions.paste(),
            actions.separator(),
                actions.cut(),
            actions.separator(),
            actions.selectAll()
            ]
        });
    },
    
    createAppMenu: () => {
        const isMac = process.platform === 'darwin'

        const template = [
        {
            label: 'File',
            submenu: [
            isMac ? { role: 'close' } : { role: 'quit' }
            ]
        },
        {
            label: 'Settings',
            submenu: [
            {
                label: 'Set IP',
                click: async () => {
                    enterIp()
                }
            }
            ]
        },
        // { role: 'editMenu' }
        {
            label: 'Edit',
            submenu: [
            { role: 'undo' },
            { role: 'redo' },
            { type: 'separator' },
            { role: 'cut' },
            { role: 'copy' },
            { role: 'paste' },
            ...(isMac
                ? [
                    { role: 'pasteAndMatchStyle' },
                    { role: 'delete' },
                    { role: 'selectAll' },
                    { type: 'separator' },
                    {
                    label: 'Speech',
                    submenu: [
                        { role: 'startSpeaking' },
                        { role: 'stopSpeaking' }
                    ]
                    }
                ]
                : [
                    { role: 'delete' },
                    { type: 'separator' },
                    { role: 'selectAll' }
                ])
            ]
        },
        {
            label: 'View',
            submenu: [
            { role: 'reload' },
            { role: 'forceReload' },
            { role: 'toggleDevTools' },
            { type: 'separator' },
            { role: 'resetZoom' },
            { role: 'zoomIn' },
            { role: 'zoomOut' },
            { type: 'separator' },
            { role: 'togglefullscreen' }
            ]
        },
        {
            role: 'Help',
            submenu: [
            {
                label: 'Learn More',
                click: async () => {
                    const { shell } = require('electron')
                    await shell.openExternal('https://gitlab.com/devapro/electron-wallet')
                }
            }
            ]
        }
        ]

        const menu = Menu.buildFromTemplate(template)
        Menu.setApplicationMenu(menu)
    }
}