
let _defaultIp = ''

module.exports = {

    setDefaultIp: (ip) => {
        _defaultIp = ip
    },

    getDefaultIp: () => _defaultIp
}