package prizm.client;


import java.util.Random;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class RandomService {
    private final static Random SECURE_RANDOM = new Random();

    public Integer[] getNumbers(int amount) throws IllegalArgumentException {

        Integer arrayOfIntegers[] = new Integer[amount];
        synchronized (SECURE_RANDOM) {
            for (int i = 0; i < amount; i++) {
                arrayOfIntegers[i] = SECURE_RANDOM.nextInt();
            }
        }
        return arrayOfIntegers;
    }
}
